﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Xml.Serialization;
using System.IO;


namespace energimaaling
{
    public class Serialiser
    {


        /*
            Console.WriteLine("Test XML Serialization.");

            ContainerData cd;
            cd.type = 0;
            

            InnerData d;
            d.test = 123;
            d.aloha = "asdfds";

            cd.element = Serialize(d);
            byte[] ser = Serialize(cd);

            Console.WriteLine("Attempt de-serializing..");
            ContainerData ret = (ContainerData)Deserialize(ser, typeof (ContainerData));

            Console.WriteLine("Got type: " + ret.ToString());
            Console.WriteLine (".type=" + ret.type.ToString ());

            Console.WriteLine ("De-serializing inner object..");
            InnerData retd = (InnerData) Deserialize (cd.element, typeof (InnerData));
            Console.WriteLine ("Contentes: " + retd.test.ToString() + ", string= " + retd.aloha);
        */

        public static byte[] Serialize(object o)
        {
            MemoryStream mo = new MemoryStream();
            XmlSerializer xs = new XmlSerializer(o.GetType());
            xs.Serialize(mo, o);

            //Console.WriteLine("Serialisert [" + o.GetType().ToString() + "] size: " + mo.Length.ToString());
            
            return mo.ToArray();
        }

        public static object Deserialize(byte[] b, Type t)
        {
            MemoryStream mi = new MemoryStream(b);
            XmlSerializer xs = new XmlSerializer(t);

            mi.Position = 0;

            //Console.WriteLine("Deserialiserer [" + t.ToString() + "] size: " + mi.Length.ToString());

            return xs.Deserialize(mi);
        }

        public static byte[] IntToSize(int sizeib)
        {
            Int16 sizei = Convert.ToInt16(sizeib);
            byte[] size = new byte[2];
            size[0] = Convert.ToByte(sizei>>4);
            size[1] = Convert.ToByte(sizei & 0x0F);

            return size;
        }

        public static int SizeToInt(byte[] size)
        {
            Int16 sizei = Convert.ToInt16(size[0]<<4);
            sizei = Convert.ToInt16(sizei + Convert.ToInt16 (size[1]));
            return sizei;
        }


    }
}
