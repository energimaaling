using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

using System.Threading;

using System.Security.Cryptography;

using System.Xml.Serialization;

namespace energimaaling 
{
	public class TCPKomm {
		/* Protokoll spesifikasjon:
		 * 
		 * All kommunikasjon foregår ved å sende ei TCPMelding struct
		 * Typen vil avgjere korleis ein kan tolke medlemmet 'data'.
		 * 
		 * 'data' vil være kryptert.
		 * 
		 */
		
		/* Sentral skal lytte på denne porten for tilkobling frå Hus */
		public const int SENTRALHUSPORT = 3000;
		
		/* Sentral skal lytte på denne porten for tilkobling frå PDA */
		public const int SENTRALPDAPORT = 3001;
		
		/* DEKLARASJONER 
		 *
		 * Type spesifikasjon
		 * 
		 */
		 
		[Serializable()]
		public enum Type { AuthQuery, AuthResponse, HusData, HusQuery, Error, Close, PdaData, PdaQuery, DEBUG };
		
		/* Data objekt */
		[Serializable()]
		public struct AuthQuery {
			public String kundenr;
			public String passord;
		}
		
		[Serializable()]
		public struct AuthResponse {
			public bool Accepted;	
		}
		
		[Serializable()]
		public struct HusData {
			string data;
		}
		
		/* Hus data sendt til PDA */
		[Serializable()]
		public struct PdaData {
			public double effekt;
			public bool reduser;
			public bool alarm;
			
			public string adresse;
			public string navn;
		}
		
		[Serializable()]
		public struct HusQuery { 
			// TODO!
		}
		
		[Serializable()]
		public struct Debug {
			public string msg;	
		}
		
		[Serializable()]
		public enum ErrorType { UnexpectedData };
		
		[Serializable()]
		public struct Error {
			public ErrorType et;
			public string msg;	
		}
		
		[Serializable()]
		public struct PdaQuery {
		}
		
		/* Sendast for å avslutte ei tilkobling skikkelig */
		[Serializable()]
		public struct Close {
			public string msg;	
		}
			
		[Serializable()]
		public struct TCPMelding
		{
			/* Tcp Type */
			public Type type;
			
			/* Struct tilsvarande type */
			public byte[] data;
		}

        /* Kryptering og sending
         * 
         * Bruk TCPKomm.SendMelding og TCPKomm.MottaMelding, desse krypterer objektet - serialiserer det og
         * sender det på nettverksstrømmen som du gir.
         * 
         * Du må lage ei TCPMelding og ein TCPMelding type; bruk Serialiser.Serialize () på det og lagre
         * det i TCPMelding.data.
         * 
         * Du kan få det ut på samme måte, ved å lage ei TCPMelding type og bruke
         * Serialiser.Deserialize (TCPMelding.data, typeof (TCPMelding typen)).
         * 
         * For å sende:
		
			// Lag autentiseringsmelding
			TCPKomm.TCPMelding tm;
			tm.type = TCPKomm.Type.AuthQuery;
			TCPKomm.AuthQuery a;
			a.kundenr = "test";
			a.passord = "testp";
         
         *  Serialiser AuthQuery og lagre i tm.data:
         
			tm.data = Serialiser.Serialize (a);
		
         *  Send på NetworkStream ns:
         
			TCPKomm.SendMelding (ns, tm);
		
         * 
         * For å motta:
         * 
			TCPKomm.TCPMelding res = TCPKomm.MottaMelding (ns);
         
         * For ei AuthQuery melding, gjer:
         
            TCPKomm.AuthQuery aq = (TCPKomm.AuthQuery)Serialiser.Deserialize(tm.data, typeof(TCPKomm.AuthQuery));
          
         * Du kan no bruke aq.kundenr og aq.passord.
		
         
         * Hugs å laste nøkkel først i kvar prosess (biblioteket har ein instans for kvar) før du begynner.
	      	
            TCPKomm.loadkeys ();
	      	
         * Nøkkel kan lagast med 'kg' på Sentral; legg key.bin og iv.bin i samme mappe som programma skal køyre
         * (Sentral, Hus, Pda).
         * 
         */


        public static void SendMelding(NetworkStream ns, TCPKomm.TCPMelding tm)
        {
            // krypterer (her blir TCPMelding serialisert)
            CryptObject c = Encrypt(tm);

            // Serialiserer CryptObjekt
            byte[] ser = Serialiser.Serialize(c);

            byte[] size = Serialiser.IntToSize(ser.Length);

            
            // sender størrelse
            ns.Write(size, 0, 2);

            /*
            Console.WriteLine("SendMelding sender: " + ser.Length.ToString());
            Console.WriteLine("Byte: " + size.ToString());
             */

            // sender objekt
            ns.Write(ser, 0, ser.Length);
            ns.Flush();
        }

        public static TCPMelding MottaMelding(NetworkStream ns)
        {
            // Vent på data
            // while (!ns.DataAvailable) Thread.Sleep(50);

            // motar størrelse
            byte[] size = new byte[2];
            
            ns.Read(size, 0, 2);
            
            int sizei = Serialiser.SizeToInt(size);

            // leser objekt
            byte[] ser = new byte[sizei];
            int r = ns.Read(ser, 0, sizei);

            /*
            Console.WriteLine("MottaMelding; fikk: " + r.ToString() + " av " + sizei.ToString());
            Console.WriteLine("Byte: " + size.ToString());
             */

            // deserialiserer cryptobjekt
            CryptObject c = (CryptObject)Serialiser.Deserialize(ser, typeof(CryptObject));

            // dekrypterer
            TCPMelding tm = Decrypt(c);
            return tm;
        }

		[Serializable()]
		public struct CryptObject
		{
			public int length;
			public byte[] encrypted;	
		}
		
		public static CryptObject Encrypt (TCPMelding tm)
		{
			if (!keyloaded) throw new Exception ("Ingen key er lasta.");
			CryptObject c = new CryptObject ();
			
			MemoryStream enc = new MemoryStream ();
			CryptoStream cStreamW = new CryptoStream (enc, ralg.CreateEncryptor (), CryptoStreamMode.Write);

            byte[] ser = Serialiser.Serialize(tm);

			c.length = (int) ser.Length;

			cStreamW.Write (ser, 0, (int) ser.Length);
			cStreamW.FlushFinalBlock (); // also .Close ()'es
			
      		enc.Position = 0; // Rewind
			c.encrypted = (byte[]) enc.ToArray ();
			return c;
		}
		
		public static TCPMelding Decrypt (CryptObject c)
		{
			if (!keyloaded) throw new Exception ("Ingen key er lasta.");
		    MemoryStream benc = new MemoryStream (c.encrypted);

			CryptoStream cStreamR = new CryptoStream (benc, ralg.CreateDecryptor (), CryptoStreamMode.Read);
			
			byte [] buenc = new byte[c.length];
			cStreamR.Read (buenc, 0, c.length);

            TCPMelding tm = (TCPMelding)Serialiser.Deserialize(buenc, typeof(TCPMelding));
			cStreamR.Close ();
			return tm;
		}
		
		
		private static bool keyloaded = false;
		private static byte[] IV;
		private static byte[] KEY;
		public static Rijndael ralg;
		
		public static void loadkeys () {
			log ("Laster Rijndaelll nøkkel..");
			try {
				/*
				 * PDA har tydeligvis problem med å laste fra fil so vi legg den inn manuelt foreløpig.
				 * 
				 * Det betyr at genkeys har ingen funksjon no; og du treng ikkje key.bin og iv.bin
				 * 
				 */
				
                byte[] key = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 };
                byte[] iv = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }; 
                ralg = Rijndael.Create();
				ralg.Key = key;
				ralg.IV = iv;
				ralg.Padding = PaddingMode.PKCS7;
				
				keyloaded = true;

			} catch {
				log ("FEIL! Kunne ikkje laste nøkkel.");	
				keyloaded = false;
			}
		}
		
		public static void genkeys () {
			log ("Lager Rijndael nøkkel..");
			ralg = Rijndael.Create ();	
			ralg.Padding = PaddingMode.PKCS7;
			
			log (".. IV");
			ralg.GenerateIV ();
			
			log (".. Key");
			ralg.GenerateKey  ();
			
			FileStream fiv = File.OpenWrite ("iv.bin");
			FileStream fkey = File.OpenWrite ("key.bin");
			
			BinaryWriter b = new BinaryWriter (fiv);
			b.Write (ralg.IV);
			
			b = new BinaryWriter (fkey);
			b.Write (ralg.Key);
			
			fiv.Close ();
			fkey.Close ();
			log ("OK.");
			
			KEY = ralg.Key;
			IV = ralg.IV;
			keyloaded = true;
		}
		
		private static void log (String s) {
			Console.WriteLine ("[TcpMelding] " + s);	
		}
	}
	
}

