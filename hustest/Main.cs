using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
		
using energimaaling;

namespace hustest
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("Hello World!");
			
			TCPKomm.loadkeys ();
			

			TcpClient tc = new TcpClient ();
			IPEndPoint ip = new IPEndPoint (IPAddress.Parse ("127.0.0.1"), TCPKomm.SENTRALHUSPORT);
			tc.Connect (ip);
			NetworkStream ns = tc.GetStream ();
			int tick = 0;
			
			// Debug	
			TCPKomm.TCPMelding td;
			td.type = TCPKomm.Type.DEBUG;
			TCPKomm.Debug dd;
			dd.msg = "Tick [" + tick.ToString () + "] Forsøker å autentisere.";
			td.data = Serialiser.Serialize (dd);
			TCPKomm.SendMelding (ns, td);
			
			
			// Autentiser
			TCPKomm.TCPMelding tm;
			tm.type = TCPKomm.Type.AuthQuery;
			TCPKomm.AuthQuery a;
			a.kundenr = "test";
			a.passord = "testp";
			tm.data = Serialiser.Serialize (a);
			
			TCPKomm.SendMelding (ns, tm);
			
			TCPKomm.TCPMelding res = TCPKomm.MottaMelding (ns);
			
			Console.WriteLine ("Got: " + res.type.ToString ());
			TCPKomm.AuthResponse ar = (TCPKomm.AuthResponse) Serialiser.Deserialize (res.data, typeof (TCPKomm.AuthResponse));
			bool auth = ar.Accepted;
			
			Console.WriteLine ("Login: " + auth.ToString ());
			
			dd.msg = "Tick [" + tick.ToString () + "] Resultat: " + auth.ToString ();
            td.data = Serialiser.Serialize(dd);
			TCPKomm.SendMelding (ns, td);
			
			// Starter sende tråd, tar ikkje i mot Close meldinger.
			
			
			while (true) {
				tm.type = TCPKomm.Type.DEBUG;
				TCPKomm.Debug d;
				d.msg = "Tick [" + tick.ToString () + "] Debug test!";
                tm.data = Serialiser.Serialize(d);
					
				try {
					TCPKomm.SendMelding (ns, tm);
					
				} catch (Exception e) {
					Console.WriteLine ("Ex: " + e.Message);	
					break;
				}
					
				
				tick++;
				Thread.Sleep (3500);
			}
			
			tc.Close ();
			
		}
	}
}

