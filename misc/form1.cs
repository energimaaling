using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using System.Xml.Serialization;
using System.Security.Cryptography;

using energimaaling;

namespace HUS_PDA
{
    public partial class Form1 : Form
    {
        public string Kundenummer;
        public string Passord;
       // public string sentralIP;
        Thread PK;

        /* Event som blir gitt ut dersom PDA_KOM har oppdatert
         * data.
         */

        public delegate void NyDataHandler ();
        public event NyDataHandler NyData;

        /* Variabler som blir oppdatert av PDA_KOM i forkant av
         * NyData event.
         *
         * Legg til for alle tekstbokser her.
         *
         */
        public string forbruk = "";


        public Form1()
        {

            InitializeComponent();
            TCPKomm.loadkeys();
            //oppretter innloggingsform
            Form2 minForm = new Form2(this);
            minForm.ShowDialog();

            /* Sett opp funksjon som reagerer på NyData event
             */

            Nydata += new NyDataHandler (OppdaterData);

            //starter tråden for PDA-kommunikasjon
            ThreadStart ts = new ThreadStart(PDA_KOM);
            PK = new Thread(ts);
            PK.Start();

        }

        private void OppdaterData ()
        {
          /* Denne funksjonen blir køyrd kvar gang NyData eventen
           * blir sendt ut.
           */


          
          /* Oppdater alle tekstbokser til å nye verdier av
           * variabler.
           */
          txt_forbruk.Text = forbruk;

        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            txt_kundenr.Text = " ";
            txt_adresse.Text = " ";
            txt_forbruk.Text = " ";
            txt_navn.Text = " ";
        }

        private void menuItem3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void PDA_KOM()
        {
            TcpClient tc = new TcpClient();
            IPEndPoint ip = new IPEndPoint(IPAddress.Parse("192.168.0.198"), TCPKomm.SENTRALPDAPORT);



                tc.Connect(ip);


              NetworkStream ns = tc.GetStream();


            int tick = 0;


            // Autentiser
            TCPKomm.TCPMelding tm;
            tm.type = TCPKomm.Type.AuthQuery;
            TCPKomm.AuthQuery a;
            a.kundenr = Kundenummer;
            a.passord = Passord;
            tm.data = Serialiser.Serialize(a);

            TCPKomm.SendMelding(ns, tm);

            TCPKomm.TCPMelding res = TCPKomm.MottaMelding(ns);
            TCPKomm.PdaData ar = (TCPKomm.PdaData)Serialiser.Deserialize(res.data, typeof(TCPKomm.PdaData));

            /* Oppdater variabler etter å ha mottatt data 
             *
             * Tilpass dette til alle variabler og riktig variabel navn.
             */ 
            forbruk = ar.forbruk;


            /* Etter å ha mottatt PdaData og oppdatert
             * variabler (effekt, adresse, etc) i Forma
             * køyr NyData event.
             */
            NyData ();




        }


    }
}
