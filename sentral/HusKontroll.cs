using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;

using System.IO;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using energimaaling;

namespace sentral
{
	public class HusKontroll
	{
		private const string husdata_uri = "husdata.bin"; // default filename for husdata
		
		
		private volatile List<Hus> HusData;
		
		
		// TCP Server
		private TcpListener husListener;
		private Thread husListenerThread;
		
		private List<HusInstans> husInstanser = new List<HusInstans>();
		
		public HusKontroll (ref List<Hus> _HusData)
		{
			log ("Starter..");
			HusData = _HusData;
			
			Hus.EffektEndring += new Hus.EffektEndringHandler (NyEffekt);
			Hus.Reduser = false;
			Hus.gjennomsnittseffekt = 0;
			
			LoadHusData ();
			
			// Start Hus TCP Server i eigen tråd
			husListenerThread = new Thread (new ThreadStart (HusListener));
			husListenerThread.Start ();
		}
		
		public void LoadHusData ()
		{
			log ("Laster husdata frå fil..");
			
			IFormatter b = new BinaryFormatter ();
			
			FileStream fs = File.OpenRead (husdata_uri);
			List<Hus> h = (List<Hus>) b.Deserialize (fs);
			HusData.Clear ();
			HusData.AddRange (h);
			fs.Close ();
			log ("Lasta: " + HusData.Count.ToString ());
		}
		
		public void WriteHusData ()
		{
			log ("Skriver husdata til fil..");	
			
			IFormatter b = new BinaryFormatter ();
			FileStream fs = File.OpenWrite (husdata_uri);
			
			b.Serialize (fs, HusData);
			fs.Close ();
		}
		
		public void Add (Hus h) {
			// TODO: Sjekk for eksisterande kundenr
			HusData.Add (h);	
		}
		
		public void Remove (string k) {
			foreach (Hus h in HusData)
				if (h.Kundenr == k)  {
					foreach (HusInstans hi in husInstanser) if (h == hi.hus) hi.Shutdown ();
					HusData.Remove (h);
					break;
				}
		}	
		
		public void ListHus () {
			foreach (Hus h in HusData) log (h.OneLine ());	
			log ("Antall: " + HusData.Count.ToString ());
		}
		
		private void HusListener () {
			log ("[HusListener] Starter HusListener server..");
			husListener = new TcpListener (IPAddress.Any, TCPKomm.SENTRALHUSPORT);
			husListener.Start ();
				
			while (true) {
				// blocks till client connects..
				TcpClient tcpClient;
				try {
					tcpClient = husListener.AcceptTcpClient ();	
				} catch {
					// socket err	
					// TODO: Send Event til HusKontroll og start på nytt dersom vi ikkje er på veg ned..
					log ("[HusListener] Shutting down!");
					break;
				}
				
				log ("[HusListener] Tilkobling akseptert, starter HusInstans..");
				HusInstans hi = new HusInstans (tcpClient, ref HusData);
				husInstanser.Add (hi);
			}
		}
		
		public void Shutdown () {
			// Stopper alle åpne husinstanser
			foreach (HusInstans hi in husInstanser) hi.Shutdown ();
			
			// Stopper lytting etter nye husinstanser
			husListener.Stop ();
			husListenerThread.Abort();
		}
		
		public double GjennomsnittEffekt () {
			return Hus.gjennomsnittseffekt;
		}
		
		private void NyEffekt () {
			Hus.gjennomsnittseffekt = 0;
			foreach (Hus h in HusData) {
				Hus.gjennomsnittseffekt += h.Effekt;
			}
			Hus.gjennomsnittseffekt /= HusData.Count;
		}
		
		private void log (String s) {
			Console.WriteLine ("[HusKontroll] " + s);	
		}
	}
}

