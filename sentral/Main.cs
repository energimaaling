using System;
using System.Threading;
using System.Collections.Generic;

using energimaaling;

namespace sentral
{
	class Sentral
	{
		private static void log (String s) {
			Console.WriteLine ("[Main] " + s);	
		}
		
		// Lagrer alle hus instanser, tilgjengelig for både HusKontroll og PdaKontroll
		private static volatile List<Hus> HusData = new List<Hus>();
		
		private static HusKontroll husK;
		private static PdaKontroll pdaK;
		
		public static void Main (string[] args)
		{
			log ("Sentral starter.");
			
			TCPKomm.loadkeys ();
			
			// Start process Huskontroll 
			husK = new HusKontroll (ref HusData);
			
			// Start process Pda
			pdaK = new PdaKontroll (ref HusData);
			
			// Start User Interface
			log ("Starter UI..");
			Thread.Sleep (1000); // let the rest come up first.. race ?
			Thread ui = new Thread (new ThreadStart (UI));
			ui.Start ();
			ui.Join ();
			
			log ("UI Finished -> shutting down..");
			husK.Shutdown ();
			pdaK.Shutdown ();
		}
		
		public static void UI () {
			log ("[UI] Ready (? for help)");
			
			bool run = true;
			const string defprompt = "[UI] > ";
			string prompt = defprompt;
			
			string response = "";
			
			while (run) {
				Console.Write (prompt);
				response = Console.ReadLine ();
				
				switch (response) {
					case "?": 	printhelp ();
							  	break;
							  	
					case "q":	run = false;
							 	break;
							 	
					case "w": 	husK.WriteHusData ();
								break;
								
					case "l": 	husK.LoadHusData ();
								break;
								
					case "p": 	husK.ListHus ();
								break;
					
					case "a":	{
									Hus h = new Hus ();
									Console.Write ("Kundenr: ");	
									h.Kundenr = Console.ReadLine ();
					
									Console.Write ("Navn: ");
									h.Navn = Console.ReadLine ();
									
									Console.Write ("Adresse: ");
									h.Adresse = Console.ReadLine ();
									
									Console.Write ("Passord: ");
									h.Passord = Console.ReadLine ();
									
									husK.Add (h);
								}
								break;
								
					case "d": 	Console.Write ("Kundenr: "); 
								husK.Remove (Console.ReadLine ());
								break;
								
					case "kg": 	TCPKomm.genkeys ();
					 			break;
					 			
					case "kl": 	TCPKomm.loadkeys ();
					 			break;
							 	
					case "":	break;
					default:	Console.WriteLine ("Unknown: Try ? for help."); 	
								break;
				}
			}
		}
		
		public static void printhelp () {
			Console.WriteLine ("Help:");
			Console.WriteLine ("? = help");
			Console.WriteLine ("w = skriv husdata");
			Console.WriteLine ("l = last husdata");
			Console.WriteLine ("p = vis husdata");
			Console.WriteLine ("a = nytt hus");
			Console.WriteLine ("d = slett hus");
			Console.WriteLine ("kg = lag nøkler");
			Console.WriteLine ("kl = les nøkler");
			Console.WriteLine ("q = quit");
		}
	}
}

