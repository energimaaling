using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;

using System.IO;

using energimaaling;

namespace sentral
{
	public class PdaKontroll
	{
		private volatile List<Hus> HusData;
		
		// TCP Server
		private TcpListener pdaListener;
		private Thread pdaListenerThread;
		
		private List<PdaInstans> pdaInstanser = new List<PdaInstans>();
		
		public PdaKontroll (ref List<Hus> _HusData)
		{
			log ("Starter..");
			HusData = _HusData;
			
			// Start PDA Tcp Server i eigen tråd
			pdaListenerThread = new Thread (new ThreadStart (PdaListener));
			pdaListenerThread.Start ();
		}
		
		private void PdaListener () {
			log ("[PdaListener] Starter PdaListener server..");
			pdaListener = new TcpListener (IPAddress.Any, TCPKomm.SENTRALPDAPORT);
			pdaListener.Start ();
				
			while (true) {
				// blocks till client connects..
				TcpClient tcpClient;
				try {
					tcpClient = pdaListener.AcceptTcpClient ();	
				} catch {
					// socket err	
					// TODO: Send Event til PdaKontroll og start på nytt dersom vi ikkje er på veg ned..
					log ("[PdaListener] Shutting down!");
					break;
				}
				
				log ("[PdaListener] Tilkobling akseptert, starter PdaInstans.. (HusData: " + HusData.Count.ToString () + ")");
				PdaInstans pi = new PdaInstans (tcpClient, ref HusData);
				pdaInstanser.Add (pi);
			}
		}
		
		private void log (String s) {
			Console.WriteLine ("[PdaKontroll] " + s);	
		}
		
		public void Shutdown () {
			// Stopper alle åpne pdainstanser
			foreach (PdaInstans pi in pdaInstanser) pi.Shutdown ();
			
			// Stopper lytting etter nye husinstanser
			pdaListener.Stop ();
			pdaListenerThread.Abort();
			
		}
	}
}

