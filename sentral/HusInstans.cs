using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Security.Cryptography;
using System.Collections.Generic;

using System.IO;

using energimaaling;

namespace sentral
{
	public class HusInstans
	{
		public enum StateType { UNCONNECTED, UNAUTHENTICATED, AUTHENTICATED };
		
		private TcpClient tcpClient;
		private NetworkStream ns;
		
		private Thread HusInsP;
		
		private StateType state = StateType.UNAUTHENTICATED;
		
		private static volatile List<Hus> HusData;
		public volatile Hus hus;
		
		
		public HusInstans (TcpClient tc, ref List<Hus> hd) 
		{
			tcpClient = tc;
			HusData = hd;
			ns = tc.GetStream ();
			
			log ("Starter tråd..");
			HusInsP = new Thread (new ThreadStart (HusInstansP));
			HusInsP.Start ();
		}
		
		private void HusInstansP ()
		{
		    /* TODO: Takle kommunikasjon frå Hus
		     * 
		     * 1. Autentiser
		     * 2. Vent på kommando
		     * 3. Oppretthold kontakt
		     * 4. Lukk og slett frå HusData liste om kontakt er broten (Event)
		     * 
		     */
		     
			
			while (true) {
				TCPKomm.TCPMelding tm;
				
				try {
					tm = TCPKomm.MottaMelding (ns);
				
				} catch (Exception e) {
					log ("Tilkobling brutt: " + e.Message);
					break;
				}

				
				if (tm.type == TCPKomm.Type.DEBUG) {
                    TCPKomm.Debug d = (TCPKomm.Debug)Serialiser.Deserialize(tm.data, typeof(TCPKomm.Debug));
					log ("[DEBUG] " + d.msg);
				} else {
					switch (state)
					{
					case StateType.UNAUTHENTICATED:
						if (tm.type == TCPKomm.Type.AuthQuery) {
                            TCPKomm.AuthQuery aq = (TCPKomm.AuthQuery)Serialiser.Deserialize(tm.data, typeof(TCPKomm.AuthQuery));
							foreach (Hus h in HusData) {
								if (h.Kundenr == aq.kundenr) {
									if (h.CheckPassword (aq.passord)) {
										hus = h;
										state = HusInstans.StateType.AUTHENTICATED;
									}
								}
							}
							
							// response
							TCPKomm.TCPMelding tr;
							tr.type = TCPKomm.Type.AuthResponse;
							TCPKomm.AuthResponse ar;
							if (state == HusInstans.StateType.AUTHENTICATED) ar.Accepted = true;
							else ar.Accepted = false;
							tr.data = Serialiser.Serialize (ar);
							
							// send
							TCPKomm.SendMelding (ns, tr);
							
							log ("Autentikasjonsforsøk, godkjent: " + ar.Accepted.ToString ());
							
						} else {
							TCPKomm.TCPMelding res = new TCPKomm.TCPMelding ();
							res.type = TCPKomm.Type.Error;
							TCPKomm.Error err = new TCPKomm.Error();
							err.et = TCPKomm.ErrorType.UnexpectedData;
                            res.data = Serialiser.Serialize(err);
							TCPKomm.SendMelding (ns, res);
						}
						break;
					}
				}
			}
			
			tcpClient.Close ();
		}
		
		public void Shutdown ()
		{							
			// response
			TCPKomm.TCPMelding tr;
			tr.type = TCPKomm.Type.Close;
			TCPKomm.Close c;
			c.msg = ("Shutting down.");
			tr.data = Serialiser.Serialize(c);
			try {
				TCPKomm.SendMelding (ns, tr);
			} catch { }
			
			tcpClient.Close ();
			HusInsP.Abort ();	
		}
		
		public StateType State {
			get { return state; }
		}
		
		private void log (String s) {
			Console.WriteLine ("[HusInstans] [" + state.ToString () + "] " + s);	
		}
	}
}

