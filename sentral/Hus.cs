using System;

namespace sentral
{
	[Serializable]
	public class Hus 
	{
		private String kundenr;
		private String navn;
		private String adresse;
		private String passord;
		
		private bool tempAlarm;
		private DateTime mottattAlarm;
		
		private double effekt;
		
		private static bool reduser;
		
		/* Blir oppdatert av HusKontroll */
		public static double gjennomsnittseffekt;
		public delegate void EffektEndringHandler ();
		public static event EffektEndringHandler EffektEndring;
		
		public Hus ()
		{
			kundenr = "";
			navn = "";
			adresse = "";
			passord = "";
			
			tempAlarm = false;
			mottattAlarm = DateTime.Now; 
			
			effekt = 0;
		}
		
		public static bool Reduser {
			get { return reduser; }
			set { reduser = value; }
		}
			
		public double Effekt {
			get { return effekt; }
			set { 
				effekt = value; 
				EffektEndring ();
			}
		}
		
		public String Kundenr {
			get { return kundenr; }
			set { kundenr = value; }
		}
		
		public String Navn {
			get { return navn; }
			set { navn = value; }
		}
		
		public String Adresse {
			get { return adresse; }
			set { adresse = value; }
		}
		
		public String Passord {
			set { passord = value; }
		}
		
		public Boolean TempAlarm {
			get { return tempAlarm; }	
			set { 
				tempAlarm = value;
				mottattAlarm = DateTime.Now;
				}
		}
		
		public DateTime MottattAlarm {
			get { return mottattAlarm; }
		}
		
		public Boolean CheckPassword (String _passord) {
			if (passord == _passord) return true;
			else return false;
		}
		
		public string OneLine () {
			return "[" + kundenr + "] " + navn + " (A: " + adresse + ") Pass: " + passord;	
		}
		
	}
}

