using System;

using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Security.Cryptography;
using System.Collections.Generic;

using System.IO;

using energimaaling;

namespace sentral
{
	public class PdaInstans
	{
		public enum StateType { UNCONNECTED, UNAUTHENTICATED, AUTHENTICATED };
		
		private TcpClient tcpClient;
		private NetworkStream ns;
		
		private Thread PdaInsP;
		
		private StateType state = StateType.UNAUTHENTICATED;
		
		private static volatile List<Hus> HusData;
		public volatile Hus hus;
		
		public PdaInstans (TcpClient tc, ref List<Hus> hd)
		{
			tcpClient = tc;
			HusData = hd;
			ns = tc.GetStream ();
		
			
			log ("Starter tråd..");
			PdaInsP = new Thread (new ThreadStart (PdaInstansP));
			PdaInsP.Start ();
			
		}
		
		private void PdaInstansP ()
		{
		    /* TODO: Takle kommunikasjon frå Pda 
		     * 
		     * 1. Autentiser
		     * 2. Vent på kommando
		     * 3. Oppretthold kontakt
		     * 4. Lukk og slett frå HusData liste om kontakt er broten (Event)
		     * 
		     */
		     
			bool run = true;
			
			while (run) {
				TCPKomm.TCPMelding tm;
				
				try {
					tm = TCPKomm.MottaMelding (ns);
					
				} catch (Exception e) {
					log ("Tilkobling brutt: " + e.Message);
					break;
				}
				
				/* Skriv DEBUG til skjerm uansett tilstand */
				if (tm.type == TCPKomm.Type.DEBUG) {
                    TCPKomm.Debug d = (TCPKomm.Debug)Serialiser.Deserialize(tm.data, typeof(TCPKomm.Debug));
					log ("[DEBUG] " + d.msg);
					
				} else {
				
					switch (state)
					{
					case StateType.UNAUTHENTICATED:
						if (tm.type == TCPKomm.Type.AuthQuery) {
							TCPKomm.AuthQuery aq = (TCPKomm.AuthQuery) Serialiser.Deserialize (tm.data, typeof (TCPKomm.AuthQuery));
							foreach (Hus h in HusData) {
								if (h.Kundenr == aq.kundenr) {
									if (h.CheckPassword (aq.passord)) {
										hus = h;
										state = StateType.AUTHENTICATED;
									}
								}
							}
							
							// response
							TCPKomm.TCPMelding tr;
							tr.type = TCPKomm.Type.AuthResponse;
							TCPKomm.AuthResponse ar;
							if (state == StateType.AUTHENTICATED) ar.Accepted = true;
							else ar.Accepted = false;
							tr.data = Serialiser.Serialize (ar);
							
							// send
						    TCPKomm.SendMelding (ns, tr);
							
							log ("Autentikasjonsforsøk, godkjent: " + ar.Accepted.ToString ());
							run = ar.Accepted; // Lukk dersom ikkje godkjent.
							
						} else {
							TCPKomm.TCPMelding res = new TCPKomm.TCPMelding ();
							res.type = TCPKomm.Type.Error;
							TCPKomm.Error err = new TCPKomm.Error();
							err.et = TCPKomm.ErrorType.UnexpectedData;
                            res.data = Serialiser.Serialize(err);
							TCPKomm.SendMelding (ns, res);
						}
						break;
						
						
					case StateType.AUTHENTICATED:
						{
							switch (tm.type)
							{
								case TCPKomm.Type.PdaQuery: 
									TCPKomm.TCPMelding tres;
									tres.type = TCPKomm.Type.PdaData;
									
									TCPKomm.PdaData pd;
									pd.adresse = hus.Adresse;
									pd.alarm = hus.TempAlarm;
									pd.effekt = hus.Effekt;
									pd.navn = hus.Navn;
									pd.reduser = Hus.Reduser;
									
									tres.data = Serialiser.Serialize (pd);
									
									TCPKomm.SendMelding (ns, tres);
							
									break;
									
								case TCPKomm.Type.Close:
									TCPKomm.Close cm = (TCPKomm.Close) Serialiser.Deserialize (tm.data, typeof (TCPKomm.Close));
									log ("Lukker: " + cm.msg);
									run = false;
									this.Shutdown ();
									break;
								default:
									/* Alle andre er feile */
									TCPKomm.TCPMelding tcl;
									tcl.type = TCPKomm.Type.Close;
									TCPKomm.Close c;
									c.msg = "Feil data.";
									tcl.data = Serialiser.Serialize (c);
									
									try {
										TCPKomm.SendMelding (ns, tcl);	
									} catch { }
							
									run = false;
									this.Shutdown ();
									break;
							}
						}
						break;
					}
				}
			}
			
			tcpClient.Close ();
		}
		
		public void Shutdown ()
		{							
			// response
			TCPKomm.TCPMelding tr;
			tr.type = TCPKomm.Type.Close;
			TCPKomm.Close c;
			c.msg = ("Shutting down.");
			tr.data = Serialiser.Serialize (c);
			try {
				TCPKomm.SendMelding (ns, tr);
			} catch { }
			
			tcpClient.Close ();
			PdaInsP.Abort ();	
		}
		
		public StateType State {
			get { return state; }
		}
		
		private void log (String s) {
			Console.WriteLine ("[PdaInstans] [" + state.ToString () + "] " + s);	
		}
	}
}

